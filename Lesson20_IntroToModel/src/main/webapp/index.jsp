<html>
<body>
    <h2>Hello SPRING!</h2>

    <br/><br/><br/>
    <h3><a href="/first/hello">Say hello</a> or <a href="/first/goodbye">Say goodbye</a></h3>
    <h3><a href="/first/hello?name=Tom&surname=Jone">Request with parameters</a></h3>

    <br/><br/><br/>
    <h3><a href="/first/calculator?a=1&b=2&action=division">Calculator with params</a></h3>
</body>
</html>

package ru.alishev.springcourse;

import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @author Neil Alishev
 */
public class TestSpring {

    public static void main(String[] args) {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(
                "applicationContext.xml"
        );

        // Music music = context.getBean("musicBean", Music.class);
        // MusicPlayer musicPlayer = new MusicPlayer(music);

        MusicPlayer musicPlayer;

        // ClassicalMusic
        musicPlayer = context.getBean("musicPlayer1", MusicPlayer.class);
        musicPlayer.playMusic();

        // RockMusic
        musicPlayer = context.getBean("musicPlayer2", MusicPlayer.class);
        musicPlayer.playMusic();


        context.close();
    }
}
